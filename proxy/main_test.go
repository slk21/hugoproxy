package main

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_HeandleAPI(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/api/", nil)
	w := httptest.NewRecorder()
	heandleAPI(w, req)

	res := w.Result()
	defer res.Body.Close()

	data, _ := io.ReadAll(res.Body)

	if string(data) != "Hello from API" {
		t.Errorf("Ожидаемый ответ: Hello from API\nПолученный ответ: %s", string(data))
	}
}

type mockHandler struct {
	HeaderSet bool
}

func (m *mockHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Устанавливаем заголовок в true
	m.HeaderSet = true
}

type testCase struct {
	api      string
	expected bool
}

func Test_ReverseProxy(t *testing.T) {

	testi := []testCase{
		{
			api:      "http://example.com/api",
			expected: true,
		},
		{
			api:      "http://example.com/",
			expected: false,
		},
		{
			api:      "http://example.com/api",
			expected: true,
		},
		{
			api:      "http://example.com/api/",
			expected: true,
		},
	}

	for _, val := range testi {
		// Создание фейкового запроса
		req := httptest.NewRequest("GET", val.api, nil)
		w := httptest.NewRecorder()

		// Создание экземпляра структуры ReverseProxy
		rp := &ReverseProxy{
			host: "localhost",
			port: "8080",
		}

		// Создание фейкового обработчика
		mockHandler := &mockHandler{}

		// Вызов метода ReverseProxy для тестирования
		rp.ReverseProxy(mockHandler).ServeHTTP(w, req)

		// Проверка, что заголовок был установлен
		if mockHandler.HeaderSet != val.expected {
			t.Errorf("Ожидаемый заголовок: %t\nПолученный заголовок: %t", val.expected, mockHandler.HeaderSet)
		}
	}

}
