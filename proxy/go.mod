module test

go 1.19

require (
	github.com/ekomobile/dadata/v2 v2.10.0
	github.com/go-chi/chi v1.5.5
)

require github.com/go-chi/chi/v5 v5.0.12 // indirect
