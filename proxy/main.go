package main

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	r := chi.NewRouter()

	r.Use(Middleware)
	r.Get("/api/", heandleAPI)

	http.ListenAndServe(":8080", r)
}

func Middleware(next http.Handler) http.Handler {
	proxrev := NewReverseProxy("hugo", "1313")

	res := proxrev.ReverseProxy(next)

	return res
}

func heandleAPI(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Hello from API")) // A
}

const content = ``

func WorkerTest() {
	t := time.NewTicker(1 * time.Second)
	var b byte = 0
	for {
		select {
		case <-t.C:
			err := os.WriteFile("/app/static/_index.md", []byte(fmt.Sprintf(content, b)), 0644)
			if err != nil {
				log.Println(err)
			}
			b++
		}
	}
}
